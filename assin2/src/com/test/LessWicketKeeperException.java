package com.test;

public class LessWicketKeeperException extends Exception
{
    public LessWicketKeeperException(String msg)
    {
        super(msg);
    }
}