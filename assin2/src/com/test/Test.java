package com.test;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
public class Test {
	
	//---------Display Choices-----------------
	public static int displayoption() {
		Scanner sc=new Scanner(System.in);
		System.out.println("\n|----------Enter your choice----------|");
		System.out.println("|-------------------------------------|");
        System.out.println("| 1: Display All Players              |\n| 2:Update Player Information By Name |\n| 3:Display Final Team                |\n| 4:Add Player Information            |\n| 5:exit                              |");
        System.out.println("|-------------------------------------|");
        int ch=sc.nextInt();
		return ch;
	}
	
	
//-----------------This function will initialize the 20 players at the start---------------------
	
    public static ArrayList<Player> initialisePlayer()
    {
        Scanner sc=new Scanner(System.in);
        ArrayList<Player> playerList=new ArrayList<>();
        String batsman="Batsman";
        String bowler="Bowler";
        Player player1 = new Player(11,"A",batsman,100,70500,400,25,32);
        Player player2 = new Player(12,"B",batsman,20,59500,400,25,32);
        Player player3 = new Player(13,"C","All-Rounder",90,10500,400,25,32);
        Player player4 = new Player(14,"D",bowler,80,15500,400,25,32);
        Player player5 = new Player(15,"E",bowler,70,13500,400,25,32);
        Player player6 = new Player(16,"F",bowler,80,38500,400,25,32);
        Player player7 = new Player(17,"G",batsman,60,29500,400,25,32);
        Player player8 = new Player(18,"H",batsman,50,91500,400,25,32);
        Player player9 = new Player(19,"I",bowler,40,85500,400,25,32);
        Player player10 = new Player(20,"J",batsman,40,75500,400,25,32);
        Player player11 = new Player(21,"K",bowler,90,92500,400,25,32);
        Player player12 = new Player(22,"L",bowler,80,16500,400,25,32);
        Player player13 = new Player(23,"M",bowler,20,7500,400,25,32);
        Player player14 = new Player(24,"N",bowler,40,8500,400,25,32);
        Player player15 = new Player(25,"O",batsman,70,72500,400,25,32);
        Player player16 = new Player(26,"P",batsman,90,62500,400,25,32);
        Player player17 = new Player(27,"Q",bowler,50,22500,400,25,32);
        Player player18 = new Player(28,"R",bowler,60,32500,400,25,32);
        Player player19 = new Player(29,"S",batsman,30,98500,400,25,32);
        Player player20 = new Player(30,"T","WicketKeeper",30,52500,400,25,32);
        playerList.add(player1);
        playerList.add(player2);
        playerList.add(player3);
        playerList.add(player4);
        playerList.add(player5);
        playerList.add(player6);
        playerList.add(player7);
        playerList.add(player8);
        playerList.add(player9);
        playerList.add(player10);
        playerList.add(player11);
        playerList.add(player12);
        playerList.add(player13);
        playerList.add(player14);
        playerList.add(player15);
        playerList.add(player16);
        playerList.add(player17);
        playerList.add(player18);
        playerList.add(player19);
        playerList.add(player20);
        return playerList;
    }
    
    //-------This function is for case 1: To display all players----------
    
    public static void displayAllPlayer(List<Player> playerList)
    {
        System.out.println("Id\tName\t Match Played\tRuns Scored\tWickets Taken\tDucks\tPlayer Type");
        Iterator<Player> itr = playerList.listIterator();
        while (itr.hasNext()) 
        {
            Player p1=itr.next();
            System.out.println(p1.getId()+"       "+p1.getName()+"\t    "+p1.getMp()+" \t"+p1.getTotalruns()+" \t\t"+p1.getWicket()+"\t\t"+p1.getDuck()+"\t"+p1.getPtype());
        }
    }
    
    //--------This function is for case 2: To Update a particular player-------
    
    public static List<Player> updatePlayer(List<Player> playerList) throws PlayerNotFoundException{
        Scanner sc=new Scanner(System.in);
    	System.out.println("Enter name of player to be updated");
        String searchName=sc.next();
        int found=0;
        Iterator<Player> itr2 = playerList.listIterator();
        while (itr2.hasNext()) 
        {
            Player p1 = itr2.next();
            if(p1.getName().equals(searchName))
            {
                found=1;
	            System.out.println("enter matches played");
	            p1.setMp(sc.nextInt());
	            System.out.println("enter runs scored");
	            p1.setHs(sc.nextInt());
	            System.out.println("enter total runs");
	            p1.setTotalruns(sc.nextInt());
	            System.out.println("enter wickets taken");
	            p1.setWicket(sc.nextInt());
	            System.out.println("enter ducks");
	            p1.setDuck(sc.nextInt());
	            System.out.println("enter player type");
	            p1.setPtype(sc.next());
            }
        }
        if(found==1)
        {
            System.out.println("Player updated");
        }
        else
        {
        	PlayerNotFoundException playerNotFoundException = new PlayerNotFoundException("Player with same name not found.");
           throw (playerNotFoundException);
        }
    	return playerList;
    }
    
    //--------This function is for case 3: To display final team---------
    
    public static void finalTeam(List<Player> playerList) throws LessBowlerException, LessWicketKeeperException{
    	String w="wicketkeeper";
        String bow="bowler";
        String bat="batsman";
        Map<Player,Integer> allPlayers=new HashMap<>();
        Iterator<Player> itr1 = playerList.listIterator();
        while (itr1.hasNext()) {
            Player p = itr1.next();
            allPlayers.put(p, (int) p.getAvg());
        }
        // Sort the list
        LinkedHashMap<Player, Integer> sortedPlayers = new LinkedHashMap<>();
        allPlayers.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .forEachOrdered(x -> sortedPlayers.put(x.getKey(), x.getValue()));
        LinkedHashMap<Player, Integer> playingEleven = new LinkedHashMap<>();
        int countBowlers=0;
        int countWicketKeeper=0;
        int totalPlayerCount=0;
        Iterator hmIterator = sortedPlayers.entrySet().iterator();
        while(hmIterator.hasNext() && totalPlayerCount<=11)
        {
            Map.Entry mapElement = (Map.Entry)hmIterator.next();
            Player key= (Player) mapElement.getKey();
            if(totalPlayerCount<7) {
                if (key.getPtype().equalsIgnoreCase(bat)) {
                    playingEleven.put(key, (int) key.getAvg());
                    totalPlayerCount++;
                } else if (key.getPtype().equalsIgnoreCase(bow)) {
                    playingEleven.put(key, (int) key.getAvg());
                    countBowlers++;
                    totalPlayerCount++;
                } else if (key.getPtype().equalsIgnoreCase(w) && countWicketKeeper < 1) {
                    playingEleven.put(key, (int) key.getAvg());
                    countWicketKeeper++;
                    totalPlayerCount++;
                }
            }
            else if(countWicketKeeper==0 && key.getPtype().equalsIgnoreCase(w)){
                    countWicketKeeper++;
                    totalPlayerCount++;
                    playingEleven.put(key,(int) key.getAvg());
            }
            else if(countBowlers<3 && key.getPtype().equalsIgnoreCase(bow)){
                countBowlers++;
                totalPlayerCount++;
                playingEleven.put(key,(int) key.getAvg());
            }
            else if(countBowlers<3 && countWicketKeeper==0 && (key.getPtype().equalsIgnoreCase(w) || key.getPtype().equalsIgnoreCase(bow)))
            {
                totalPlayerCount++;
                playingEleven.put(key, (int) key.getAvg());
                if(key.getPtype().equalsIgnoreCase(w))
                    countWicketKeeper++;
                if(key.getPtype().equals(bow))
                    countBowlers++;
            }
            if(countBowlers>=3 && countWicketKeeper>=1){
                playingEleven.put(key, (int) key.getAvg());
                totalPlayerCount++;
            }
        }
    	
        ArrayList<Player> finalList=new ArrayList<>();
        for(Player key:playingEleven.keySet())
        {
            finalList.add(key);
        }
        displayAllPlayer(finalList);
    	
    	
    	
    	//Checking for bowler and wicket keeper count and throwing exception
    	int countBowlers1 = 0;
        int countWicketKeeper1 = 0;
        Iterator<Player> itr = playerList.listIterator();
        while (itr.hasNext()) {
            Player p = itr.next();
            if (p.getPtype().equalsIgnoreCase("bowler"))
                countBowlers1++;
            if (p.getPtype().equalsIgnoreCase("wicketKeeper"))
                countWicketKeeper1++;
        }
        if (countBowlers1 < 3) {
            LessBowlerException lbe = new LessBowlerException("Number of Bowlers are lass than 3");
            throw(lbe);
        }
        if (countWicketKeeper1 > 1) {
            LessWicketKeeperException lwe = new LessWicketKeeperException("Number of Wicket Keepers is more than 1");
            throw(lwe);
        }
    }
    
    //--------This function is for case 4: To add another player information---------
    
    public static List<Player> createPlayer(List<Player> playerList){
        Scanner sc=new Scanner(System.in);
        String name;
        int mp;
        int id;
        int duck;
        int wicket;
        int totalruns;
        String ptype;
    	System.out.println("enter details for player ");
        System.out.println("enter id");
        id=sc.nextInt();
        System.out.println("enter name");
        name=sc.next();
        System.out.println("enter match played");
        mp=sc.nextInt();
        System.out.println("enter total runs scored");
        totalruns=sc.nextInt();
        System.out.println("enter wickets taken");
        wicket=sc.nextInt();
        System.out.println("enter out on zero");
        duck=sc.nextInt();
        System.out.println("enter player type");
        ptype=sc.next();
        float avg=(float)totalruns/mp;
        Player x=new Player(id,name,ptype,avg,totalruns,mp,wicket,duck);
        playerList.add(x);
    	return playerList;
    }
    
 //----------------------------------------------------------------------------------------
 //                              MAIN FUNCTION
 //-------------------------------------------------------------------------------------------
    public static void main(String[] args) throws PlayerNotFoundException,LessBowlerException, LessWicketKeeperException   {
        Scanner sc=new Scanner(System.in);
        ArrayList<Player> playerList = initialisePlayer();
        int ch=displayoption();
        while(ch!=5) 
        {
        switch (ch)
        {
            case 1:
            	displayAllPlayer(playerList);
                ch=displayoption();
                break;
            case 2:
            	updatePlayer(playerList);
                ch=displayoption();
                break;
            case 3:
            	 finalTeam(playerList);
                 ch=displayoption();
                 break;
            case 4:
            	createPlayer(playerList);
            	ch=displayoption();
            	break;
            default :
            	System.out.println("Wrong Entry");
            	ch=displayoption();
            	break;
        }
       }
    }
}