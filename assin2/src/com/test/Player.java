package com.test;

public class Player
{
	String name;
	int mp;
	int hs;
	float avg;
	int id;
	int duck;
	int wicket;
	int totalruns;
	String ptype;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getMp() {
		return mp;
	}
	public void setMp(int mp) {
		this.mp = mp;
	}
	public int getHs() {
		return hs;
	}
	public void setHs(int hs) {
		this.hs = hs;
	}
	public float getAvg() {
		return avg;
	}
	public void setAvg(float avg) {
		this.avg = avg;
	}
	public int getDuck() {
		return duck;
	}
	public void setDuck(int duck) {
		this.duck = duck;
	}
	public int getWicket() {
		return wicket;
	}
	public void setWicket(int wicket) {
		this.wicket = wicket;
	}
	
	public int getTotalruns() {
		return totalruns;
	}
	public void setTotalruns(int totalruns) {
		this.totalruns = totalruns;
	}
	public String getPtype() {
		return ptype;
	}
	public void setPtype(String ptype) {
		this.ptype = ptype;
	}
	public Player(int id,String name,String ptype,float avg,int totalruns,int mp,int wicket,int duck) {
		super();
		this.id=id;
		this.name = name;
		this.mp = mp;
		this.avg = avg;
		this.duck = duck;
		this.wicket = wicket;
		this.totalruns = totalruns;
		this.ptype = ptype;
		this.avg=avg;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public Player() {
		// TODO Auto-generated constructor stub
	}
	
}
