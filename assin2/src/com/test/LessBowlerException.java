package com.test;

public class LessBowlerException extends Exception{
    public LessBowlerException(String msg){
        super(msg);
    }
}