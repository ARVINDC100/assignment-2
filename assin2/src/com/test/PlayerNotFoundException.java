package com.test;

public class PlayerNotFoundException extends Exception
{
    public PlayerNotFoundException(String s) {
        super(s);
    }
}
